class TidyCodeDataSection {
  String dataTitle;
  String tidyCodeData;

  TidyCodeDataSection({this.dataTitle, this.tidyCodeData});

  factory TidyCodeDataSection.fromMap(Map<String, dynamic> jsonData) {
    return TidyCodeDataSection(
      dataTitle: jsonData["title"],
      tidyCodeData: jsonData["codebox"],
    );
  }
}
