import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../LanguageData.dart';
import '../Widgets.dart';

class PythonSearchPage extends StatefulWidget {
  @override
  _PythonSearchPageState createState() => _PythonSearchPageState();
}

class _PythonSearchPageState extends State<PythonSearchPage> {
  List<TidyCodeDataSection> tidyCodeDataSectionOne = new List();
  List<TidyCodeDataSection> tidyCodeDataSectionTwo = new List();
  List<TidyCodeDataSection> tidyCodeDataSectionThree = new List();
  List<TidyCodeDataSection> tidyCodeDataSectionFour = new List();
  List<TidyCodeDataSection> tidyCodeDataSectionFive = new List();
  List<TidyCodeDataSection> tidyCodeDataSectionSix = new List();
  List<TidyCodeDataSection> totalTidyCodeList = new List();
  List<TidyCodeDataSection> searchResult = new List();
  TextEditingController controller = new TextEditingController();

  getLanguageData() async {
    //SectionOne - Arrays
    var responseOfSectionOne = await http.get(
      "https://raw.githubusercontent.com/Beyond-Intelligence/tidycode-json/master/python/array.json",
    );

    Map<String, dynamic> jsonDataSectionOne =
        jsonDecode(responseOfSectionOne.body);

    jsonDataSectionOne["tidyList"].forEach(
      (element) {
        TidyCodeDataSection sectionDataOne = new TidyCodeDataSection();
        sectionDataOne = TidyCodeDataSection.fromMap(element);
        tidyCodeDataSectionOne.add(sectionDataOne);
      },
    );

    // SectionTwo - files
    var responseOfSectionTwo = await http.get(
        'https://raw.githubusercontent.com/Beyond-Intelligence/tidycode-json/master/python/files.json');

    Map<String, dynamic> jsonDataSectionTwo =
        jsonDecode(responseOfSectionTwo.body);

    jsonDataSectionTwo["tidyList"].forEach(
      (element) {
        TidyCodeDataSection sectionDataTwo = new TidyCodeDataSection();
        sectionDataTwo = TidyCodeDataSection.fromMap(element);
        tidyCodeDataSectionTwo.add(sectionDataTwo);
      },
    );

    //SectionThree - list
    var responseOfSectionThree = await http.get(
        'https://raw.githubusercontent.com/Beyond-Intelligence/tidycode-json/master/python/list.json');

    Map<String, dynamic> jsonDataSectionThree =
        jsonDecode(responseOfSectionThree.body);

    jsonDataSectionThree["tidyList"].forEach(
      (element) {
        TidyCodeDataSection sectionDataThree = new TidyCodeDataSection();
        sectionDataThree = TidyCodeDataSection.fromMap(element);
        tidyCodeDataSectionThree.add(sectionDataThree);
      },
    );

    //SectionFour - random
    var responseOfSectionFour = await http.get(
        'https://raw.githubusercontent.com/Beyond-Intelligence/tidycode-json/master/python/random.json');

    Map<String, dynamic> jsonDataSectionFour =
        jsonDecode(responseOfSectionFour.body);

    jsonDataSectionFour["tidyList"].forEach(
      (element) {
        TidyCodeDataSection sectionDataFour = new TidyCodeDataSection();
        sectionDataFour = TidyCodeDataSection.fromMap(element);
        tidyCodeDataSectionFour.add(sectionDataFour);
      },
    );

    //SectionFive - string
    var responseOfSectionFive = await http.get(
        'https://raw.githubusercontent.com/Beyond-Intelligence/tidycode-json/master/python/string.json');

    Map<String, dynamic> jsonDataSectionFive =
        jsonDecode(responseOfSectionFive.body);

    jsonDataSectionFive["tidyList"].forEach(
      (element) {
        TidyCodeDataSection sectionDataFive = new TidyCodeDataSection();
        sectionDataFive = TidyCodeDataSection.fromMap(element);
        tidyCodeDataSectionFive.add(sectionDataFive);
      },
    );

    // SectionSix - time
    var responseOfSectionSix = await http.get(
      "https://raw.githubusercontent.com/Beyond-Intelligence/tidycode-json/master/python/time.json",
    );

    Map<String, dynamic> jsonDataSectionSix =
        jsonDecode(responseOfSectionSix.body);

    jsonDataSectionSix["tidyList"].forEach(
      (element) {
        TidyCodeDataSection sectionDataSix = new TidyCodeDataSection();
        sectionDataSix = TidyCodeDataSection.fromMap(element);
        tidyCodeDataSectionSix.add(sectionDataSix);
      },
    );

    // Sum it up all
    totalTidyCodeList = tidyCodeDataSectionOne +
        tidyCodeDataSectionTwo +
        tidyCodeDataSectionThree +
        tidyCodeDataSectionFour +
        tidyCodeDataSectionFive +
        tidyCodeDataSectionSix;
  }

  @override
  void initState() {
    getLanguageData();
    super.initState();
  }

  onSearchTextChanged(String text) {
    searchResult.clear();
    if (text.isEmpty) {
      setState(
        () {},
      );
      return;
    }

    totalTidyCodeList.forEach(
      (languageDataModel) {
        if (languageDataModel.dataTitle.toLowerCase().contains(
              text.toLowerCase(),
            )) searchResult.add(languageDataModel);
      },
    );

    setState(
      () {},
    );
  }

  Widget theSearchBox() {
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: MediaQuery.of(context).size.width * .10,
      ),
      child: ListTile(
        title: TextField(
          autofocus: true,
          textAlign: TextAlign.center,
          controller: controller,
          cursorColor: Colors.white,
          style: TextStyle(
            color: Colors.white,
          ),
          decoration: InputDecoration(
            hintText: 'Search',
            hintStyle: TextStyle(
              color: Colors.white54,
            ),
            border: InputBorder.none,
            fillColor: Colors.white,
          ),
          onChanged: onSearchTextChanged,
        ),
        trailing: IconButton(
          icon: Icon(
            Icons.cancel,
            color: Colors.red[800],
          ),
          splashRadius: 18,
          splashColor: Colors.red,
          onPressed: () {
            controller.clear();
            onSearchTextChanged('');
          },
        ),
      ),
    );
  }

  Widget languageDataSearchList() {
    return Container(
      child: ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        physics: ClampingScrollPhysics(),
        itemCount: searchResult.length,
        itemBuilder: (context, index) {
          return TidyCodeTile(
            theTitle: searchResult[index].dataTitle,
            theData: searchResult[index].tidyCodeData,
          );
        },
      ),
    );
  }

  Widget theLanguageDataBody() {
    return Container(
      child: languageDataSearchList(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: theSearchBox(),
        centerTitle: true,
        backgroundColor: Colors.blueGrey[800].withAlpha(245),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Container(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 5,
              ),
              theLanguageDataBody(),
              SizedBox(
                height: 18,
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: kIsWeb
          ? Container(
              height: AppBar().preferredSize.height,
              child: theFooter(),
            )
          : null,
    );
  }
}
