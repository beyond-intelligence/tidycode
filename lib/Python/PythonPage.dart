import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../Widgets.dart';
import '../LanguageData.dart';

class PythonLandingPage extends StatelessWidget {
  final List<TidyCodeDataSection> tidyCodeDataSectionOne = new List();
  final List<TidyCodeDataSection> tidyCodeDataSectionTwo = new List();
  final List<TidyCodeDataSection> tidyCodeDataSectionThree = new List();
  final List<TidyCodeDataSection> tidyCodeDataSectionFour = new List();
  final List<TidyCodeDataSection> tidyCodeDataSectionFive = new List();
  final List<TidyCodeDataSection> tidyCodeDataSectionSix = new List();

  // Arrays
  Future<Widget> sectionOneList() async {
    var responseOfSectionOne = await http.get(
      "https://raw.githubusercontent.com/Beyond-Intelligence/tidycode-json/master/python/array.json",
    );

    Map<String, dynamic> jsonDataSectionOne =
        jsonDecode(responseOfSectionOne.body);

    /*
    Empty the list before adding item from json.
    Fixes repetitive items upon opeing the page multiple times.
    */
    tidyCodeDataSectionOne.clear();
    jsonDataSectionOne["tidyList"].forEach(
      (element) {
        TidyCodeDataSection sectionDataOne = new TidyCodeDataSection();
        sectionDataOne = TidyCodeDataSection.fromMap(element);
        tidyCodeDataSectionOne.add(sectionDataOne);
      },
    );

    return ListView.builder(
      shrinkWrap: true,
      scrollDirection: Axis.vertical,
      physics: ClampingScrollPhysics(),
      itemCount: tidyCodeDataSectionOne.length,
      itemBuilder: (context, index) {
        return TidyCodeTile(
          theTitle: tidyCodeDataSectionOne[index].dataTitle,
          theData: tidyCodeDataSectionOne[index].tidyCodeData,
        );
      },
    );
  }

  //SectionTwo - files
  Future<Widget> sectionTwoList() async {
    var responseOfSectionTwo = await http.get(
        'https://raw.githubusercontent.com/Beyond-Intelligence/tidycode-json/master/python/files.json');

    Map<String, dynamic> jsonDataSectionTwo =
        jsonDecode(responseOfSectionTwo.body);

    /*
    Empty the list before adding item from json.
    Fixes repetitive items upon opeing the page multiple times.
    */
    tidyCodeDataSectionTwo.clear();
    jsonDataSectionTwo["tidyList"].forEach(
      (element) {
        TidyCodeDataSection sectionDataTwo = new TidyCodeDataSection();
        sectionDataTwo = TidyCodeDataSection.fromMap(element);
        tidyCodeDataSectionTwo.add(sectionDataTwo);
      },
    );

    return ListView.builder(
      shrinkWrap: true,
      scrollDirection: Axis.vertical,
      physics: ClampingScrollPhysics(),
      itemCount: tidyCodeDataSectionTwo.length,
      itemBuilder: (context, index) {
        return TidyCodeTile(
          theTitle: tidyCodeDataSectionTwo[index].dataTitle,
          theData: tidyCodeDataSectionTwo[index].tidyCodeData,
        );
      },
    );
  }

  //SectionThree - list
  Future<Widget> sectionThreeList() async {
    var responseOfSectionThree = await http.get(
        'https://raw.githubusercontent.com/Beyond-Intelligence/tidycode-json/master/python/list.json');

    Map<String, dynamic> jsonDataSectionThree =
        jsonDecode(responseOfSectionThree.body);

    /*
    Empty the list before adding item from json.
    Fixes repetitive items upon opeing the page multiple times.
    */
    tidyCodeDataSectionThree.clear();
    jsonDataSectionThree["tidyList"].forEach(
      (element) {
        TidyCodeDataSection sectionDataThree = new TidyCodeDataSection();
        sectionDataThree = TidyCodeDataSection.fromMap(element);
        tidyCodeDataSectionThree.add(sectionDataThree);
      },
    );

    return ListView.builder(
      shrinkWrap: true,
      scrollDirection: Axis.vertical,
      physics: ClampingScrollPhysics(),
      itemCount: tidyCodeDataSectionThree.length,
      itemBuilder: (context, index) {
        return TidyCodeTile(
          theTitle: tidyCodeDataSectionThree[index].dataTitle,
          theData: tidyCodeDataSectionThree[index].tidyCodeData,
        );
      },
    );
  }

  //SectionFour - random
  Future<Widget> sectionFourList() async {
    var responseOfSectionFour = await http.get(
        'https://raw.githubusercontent.com/Beyond-Intelligence/tidycode-json/master/python/random.json');

    Map<String, dynamic> jsonDataSectionFour =
        jsonDecode(responseOfSectionFour.body);

    /*
    Empty the list before adding item from json.
    Fixes repetitive items upon opeing the page multiple times.
    */
    tidyCodeDataSectionFour.clear();
    jsonDataSectionFour["tidyList"].forEach(
      (element) {
        TidyCodeDataSection sectionDataFour = new TidyCodeDataSection();
        sectionDataFour = TidyCodeDataSection.fromMap(element);
        tidyCodeDataSectionFour.add(sectionDataFour);
      },
    );

    return ListView.builder(
      shrinkWrap: true,
      scrollDirection: Axis.vertical,
      physics: ClampingScrollPhysics(),
      itemCount: tidyCodeDataSectionFour.length,
      itemBuilder: (context, index) {
        return TidyCodeTile(
          theTitle: tidyCodeDataSectionFour[index].dataTitle,
          theData: tidyCodeDataSectionFour[index].tidyCodeData,
        );
      },
    );
  }

  //SectionFive - string
  Future<Widget> sectionFiveList() async {
    var responseOfSectionFive = await http.get(
        'https://raw.githubusercontent.com/Beyond-Intelligence/tidycode-json/master/python/string.json');

    Map<String, dynamic> jsonDataSectionFive =
        jsonDecode(responseOfSectionFive.body);

    /*
    Empty the list before adding item from json.
    Fixes repetitive items upon opeing the page multiple times.
    */
    tidyCodeDataSectionFive.clear();
    jsonDataSectionFive["tidyList"].forEach(
      (element) {
        TidyCodeDataSection sectionDataFive = new TidyCodeDataSection();
        sectionDataFive = TidyCodeDataSection.fromMap(element);
        tidyCodeDataSectionFive.add(sectionDataFive);
      },
    );

    return ListView.builder(
      shrinkWrap: true,
      scrollDirection: Axis.vertical,
      physics: ClampingScrollPhysics(),
      itemCount: tidyCodeDataSectionFive.length,
      itemBuilder: (context, index) {
        return TidyCodeTile(
          theTitle: tidyCodeDataSectionFive[index].dataTitle,
          theData: tidyCodeDataSectionFive[index].tidyCodeData,
        );
      },
    );
  }

  // SectionSix - time
  Future<Widget> sectionSixList() async {
    var responseOfSectionSix = await http.get(
      "https://raw.githubusercontent.com/Beyond-Intelligence/tidycode-json/master/python/time.json",
    );

    Map<String, dynamic> jsonDataSectionSix =
        jsonDecode(responseOfSectionSix.body);

    /*
    Empty the list before adding item from json.
    Fixes repetitive items upon opeing the page multiple times.
    */
    tidyCodeDataSectionSix.clear();
    jsonDataSectionSix["tidyList"].forEach(
      (element) {
        TidyCodeDataSection sectionDataSix = new TidyCodeDataSection();
        sectionDataSix = TidyCodeDataSection.fromMap(element);
        tidyCodeDataSectionSix.add(sectionDataSix);
      },
    );

    return ListView.builder(
      shrinkWrap: true,
      scrollDirection: Axis.vertical,
      physics: ClampingScrollPhysics(),
      itemCount: tidyCodeDataSectionSix.length,
      itemBuilder: (context, index) {
        return TidyCodeTile(
          theTitle: tidyCodeDataSectionSix[index].dataTitle,
          theData: tidyCodeDataSectionSix[index].tidyCodeData,
        );
      },
    );
  }

  languageDataMainList() {
    return Column(
      children: [
        Container(
          child: Column(
            children: [
              //Section 1 - Arrays
              SizedBox(
                height: 18,
              ),
              SectionTitle(
                theSectionTitle: "Arrays",
              ),
              Center(
                child: FutureBuilder<Widget>(
                  future: sectionOneList(),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.done) {
                      return Center(
                        child: snapshot.data,
                      );
                    } else if (snapshot.hasError) {
                      return Text(
                        "${snapshot.error}",
                      );
                    } else {
                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Icon(Icons.code),
                      );
                    }
                  },
                ),
              ),
              SizedBox(
                height: 18,
              ),
              //Section 2 - Files
              SectionTitle(
                theSectionTitle: "Files",
              ),
              Center(
                child: FutureBuilder<Widget>(
                  future: sectionTwoList(),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.done) {
                      return Center(
                        child: snapshot.data,
                      );
                    } else if (snapshot.hasError) {
                      return Text(
                        "${snapshot.error}",
                      );
                    } else {
                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Icon(Icons.code),
                      );
                    }
                  },
                ),
              ),
              SizedBox(
                height: 18,
              ),
              //Section 3 - Lists
              SectionTitle(
                theSectionTitle: "List",
              ),
              Center(
                child: FutureBuilder<Widget>(
                  future: sectionThreeList(),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.done) {
                      return Center(
                        child: snapshot.data,
                      );
                    } else if (snapshot.hasError) {
                      return Text(
                        "${snapshot.error}",
                      );
                    } else {
                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Icon(Icons.code),
                      );
                    }
                  },
                ),
              ),
              SizedBox(
                height: 18,
              ),
              //Section 4 - Random
              SectionTitle(
                theSectionTitle: "Random",
              ),
              Center(
                child: FutureBuilder<Widget>(
                  future: sectionFourList(),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.done) {
                      return Center(
                        child: snapshot.data,
                      );
                    } else if (snapshot.hasError) {
                      return Text(
                        "${snapshot.error}",
                      );
                    } else {
                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Icon(Icons.code),
                      );
                    }
                  },
                ),
              ),
              SizedBox(
                height: 18,
              ),
              //Section 5 - Strings
              SectionTitle(
                theSectionTitle: "Strings",
              ),
              Center(
                child: FutureBuilder<Widget>(
                  future: sectionFiveList(),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.done) {
                      return Center(
                        child: snapshot.data,
                      );
                    } else if (snapshot.hasError) {
                      return Text(
                        "${snapshot.error}",
                      );
                    } else {
                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Icon(Icons.code),
                      );
                    }
                  },
                ),
              ),
              SizedBox(
                height: 18,
              ),
              //Section 6 - Time
              SectionTitle(
                theSectionTitle: "Time",
              ),
              Center(
                child: FutureBuilder<Widget>(
                  future: sectionSixList(),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.done) {
                      return Center(
                        child: snapshot.data,
                      );
                    } else if (snapshot.hasError) {
                      return Text(
                        "${snapshot.error}",
                      );
                    } else {
                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Icon(Icons.code),
                      );
                    }
                  },
                ),
              ),
              SizedBox(
                height: 18,
              ),
            ],
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Python',
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.blueGrey[800].withAlpha(245),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Container(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 5,
              ),
              languageDataMainList(),
            ],
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.miniEndFloat,
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.search),
        backgroundColor: Colors.blueGrey[800],
        elevation: 0,
        onPressed: () {
          Navigator.pushNamed(context, '/python/search');
        },
      ),
      bottomNavigationBar: kIsWeb
          ? Container(
              height: AppBar().preferredSize.height,
              child: theFooter(),
            )
          : null,
    );
  }
}
