import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:clipboard/clipboard.dart';
import 'package:flutter/painting.dart';
import 'package:footer/footer.dart';
import 'package:fluttertoast/fluttertoast.dart';

class TidyLangs extends StatelessWidget {
  final String theLang;
  final dynamic toPage;
  TidyLangs({@required this.theLang, this.toPage});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: MediaQuery.of(context).size.width * .30,
        vertical: 8,
      ),
      width: 10,
      child: MaterialButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(14),
        ),
        onPressed: () {
          Navigator.pushNamed(context, '$toPage');
        },
        child: Text(
          theLang,
          style: TextStyle(
            color: Colors.black87,
            fontWeight: FontWeight.bold,
          ),
          textAlign: TextAlign.center,
        ),
        color: Colors.white,
        padding: EdgeInsets.symmetric(
          vertical: MediaQuery.of(context).size.height * .012,
          horizontal: MediaQuery.of(context).size.width * .012,
        ),
      ),
    );
  }
}

class SectionTitle extends StatelessWidget {
  final String theSectionTitle;
  SectionTitle({this.theSectionTitle});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: 10,
      ),
      height: 45,
      width: MediaQuery.of(context).size.width / 3.5,
      decoration: BoxDecoration(
        color: Colors.blueGrey[800],
        borderRadius: BorderRadius.circular(9),
      ),
      child: Center(
        child: Text(
          theSectionTitle,
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontSize: 15,
          ),
        ),
      ),
    );
  }
}

class TidyCodeTile extends StatelessWidget {
  final String theTitle, theData;
  TidyCodeTile({@required this.theTitle, this.theData});

  void _theCopyDialog(context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return TheTidyCodeDialog(
          theCopyData: theData,
          theTitle: theTitle,
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: MediaQuery.of(context).size.width * .20,
        vertical: 8,
      ),
      width: 10,
      child: MaterialButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(14),
        ),
        onPressed: () {
          _theCopyDialog(context);
        },
        child: Text(
          theTitle,
          style: TextStyle(
            color: Colors.black87,
            fontWeight: FontWeight.bold,
          ),
          textAlign: TextAlign.center,
        ),
        color: Colors.white,
        padding: EdgeInsets.symmetric(
          vertical: MediaQuery.of(context).size.height * .012,
          horizontal: MediaQuery.of(context).size.width * .012,
        ),
      ),
    );
  }
}

class TheTidyCodeDialog extends StatelessWidget {
  final String theTitle, theCopyData;
  TheTidyCodeDialog({@required this.theCopyData, this.theTitle});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(18),
      ),
      backgroundColor: Colors.white,
      content: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.symmetric(
                horizontal: 8,
                vertical: 8,
              ),
              padding: EdgeInsets.symmetric(
                horizontal: 8,
                vertical: 8,
              ),
              child: Text(
                theTitle,
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Container(
                    alignment: Alignment.centerLeft,
                    margin: EdgeInsets.symmetric(
                      horizontal: 8,
                      vertical: 8,
                    ),
                    padding: EdgeInsets.symmetric(
                      horizontal: 8,
                      vertical: 18,
                    ),
                    decoration: BoxDecoration(
                      color: Colors.blueGrey[700],
                      borderRadius: BorderRadius.circular(14),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.5),
                          spreadRadius: 2,
                          blurRadius: 7,
                          offset: Offset(0, 1), // changes position of shadow
                        ),
                      ],
                    ),
                    child: Text(
                      theCopyData,
                      style: TextStyle(
                        fontStyle: FontStyle.italic,
                        color: Colors.white70,
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(
                    horizontal: 8,
                    vertical: 8,
                  ),
                  padding: EdgeInsets.symmetric(
                    horizontal: 8,
                    vertical: 8,
                  ),
                  child: FloatingActionButton(
                    child: Icon(
                      Icons.content_copy,
                    ),
                    backgroundColor: Colors.white,
                    foregroundColor: Colors.black54,
                    hoverElevation: 10,
                    onPressed: () {
                      FlutterClipboard.copy(theCopyData);
                      Fluttertoast.showToast(
                          msg: "Copied to the clipboard",
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.BOTTOM,
                          webPosition: "center",
                          webBgColor: "#37474F",
                          backgroundColor: Colors.blueGrey[800],
                          textColor: Colors.white,
                          fontSize: 14.0);
                    },
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

Widget theFooter() {
  return Center(
    child: Footer(
      backgroundColor: Colors.blueGrey[800].withAlpha(245),
      child: Padding(
        padding: EdgeInsets.all(15.0),
        child: Text(
          '© 2020, Beyond Intelligence',
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    ),
  );
}
