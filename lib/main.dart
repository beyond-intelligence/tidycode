import 'package:TidyCode/Python/PythonPage.dart';
import 'package:TidyCode/Python/PythonSearchPage.dart';
import 'package:flutter/material.dart';
import 'LandingPage.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: '/',
      routes: {
        '/': (context) => LandingPage(),
        '/python': (context) => PythonLandingPage(),
        '/python/search': (context) => PythonSearchPage(),
      },
      title: 'Tidy Code',
      debugShowCheckedModeBanner: false,
    );
  }
}
