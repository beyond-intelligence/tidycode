import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'LanguageList.dart';
import 'Widgets.dart';

class LandingPage extends StatefulWidget {
  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  List<Languages> languageList = new List();

  @override
  void initState() {
    languageList = getlanguageList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Image(
          fit: BoxFit.contain,
          height: AppBar().preferredSize.height * .5,
          image: AssetImage("assets/images/logoLanding.png"),
        ),
        centerTitle: true,
        backgroundColor: Colors.blueGrey[800].withAlpha(245),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        physics: ClampingScrollPhysics(),
        child: Container(
          margin: EdgeInsets.all(10),
          height: MediaQuery.of(context).size.height,
          color: Colors.white,
          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                child: ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.vertical,
                  physics: ClampingScrollPhysics(),
                  itemCount: languageList.length,
                  itemBuilder: (context, index) {
                    return TidyLangs(
                      theLang: languageList[index].languageName,
                      toPage: languageList[index].languagePage,
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
      extendBody: true,
      extendBodyBehindAppBar: true,
      bottomNavigationBar: kIsWeb
          ? Container(
              height: AppBar().preferredSize.height,
              child: theFooter(),
            )
          : null,
    );
  }
}
