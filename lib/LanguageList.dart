class Languages {
  String languageName;
  dynamic languagePage;
}

List<Languages> getlanguageList() {
  List<Languages> languageList = new List();
  Languages languageModel = new Languages();

  languageModel.languageName = "Python";
  languageModel.languagePage = "/python";
  languageList.add(languageModel);
  languageModel = new Languages();

  // languageModel.languageName = "Scala";
  // languageModel.languagePage = ScalaLandingPage();
  // languageList.add(languageModel);
  // languageModel = new Languages();

  return languageList;
}
